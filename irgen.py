# SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
# SPDX-FileCopyrightText: 2022 Linus Jahn <lnj@kaidan.im>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

import xsd
import ir
from case_conversion import *


class State:
    namespaces: dict[str, ir.ComponentNamespace]
    enums: list[ir.EnumType]
    elements: list[ir.Element]

    def __init__(self):
        self.namespaces = dict()
        self.enums = []
        self.elements = []


def map_type(state: State, xml_type: str) -> tuple[str, str]:
    def map():
        match xml_type:
            case "xs:string":
                return "QString"
            case "xs:decimal":
                return "double", "DoubleSerDe"
            case "xs:byte":
                return "int8_t", "IntegerSerDe<int8_t>"
            case "xs:short":
                return "int16_t", "IntegerSerDe<int16_t>"
            case "xs:int":
                return "int32_t", "IntegerSerDe<int32_t>"
            case "xs:long":
                return "int64_t", "IntegerSerDe<int64_t>"
            case "xs:unsignedByte":
                return "uint8_t", "IntegerSerDe<uint8_t>"
            case "xs:unsignedShort":
                return "uint16_t", "IntegerSerDe<uint16_t>"
            case "xs:unsignedInt":
                return "uint32_t", "IntegerSerDe<uint32_t>"
            case "xs:unsignedLong":
                return "uint64_t", "IntegerSerDe<uint64_t>"
            # unlimited types: we use integers of 64 bits
            case "xs:integer":
                return map_type(state, "xs:long")
            case "xs:negativeInteger":
                # TODO: Only allow values < 0
                return map_type(state, "xs:long")
            case "xs:nonPositiveInteger":
                # TODO: Only allow values <= 0
                return map_type(state, "xs:unsignedLong")
            case "xs:positiveInteger":
                # TODO: positiveInteger must not be 0
                return map_type(state, "xs:unsignedLong")
            case "xs:nonNegativeInteger":
                return map_type(state, "xs:unsignedLong")
            case str() as a:
                print(f"Could not match type: {a}. Falling back to QString.")
                return "QString"
    match map():
        case str() as s:
            return s, f"StringSerDe<{s}>"
        case tuple() as t:
            return t


def gen_element_type(state: State, xml_type: str, xml_content_name: str):
    c = ir.ElementContent()
    c.type, c.serde_type = map_type(state, xml_type)
    c.name = to_camel_case(xml_content_name)
    return c


def collect_elements(state: State, obj: any, xmlns: str):
    cpp_namespace = state.namespaces[xmlns].namespace
    match obj:
        case xsd.Element() as xml_el:
            xml_el: xsd.Element

            if xml_el.ref:
                # this is just a reference to an already defined element
                return state
            if not xml_el.name:
                print("Warning: Missing name on element!")
                return state
            print(f"Element(name={xml_el.name})")

            if not xml_el.complex_type and not xml_el.simple_type and not xml_el.ref:
                # early exit: this element will be inlined into the parent struct
                return state

            el = ir.Element()
            el.xml_name = xml_el.name
            el.xmlns = xmlns
            el.namespace = state.namespaces[xmlns].namespace
            el.name = to_upper_camel_case(xml_el.name)

            if xml_el.type:
                el.content = gen_element_type(state, xml_el.type, xml_el.content_name)

            if xml_el.simple_type:
                if xml_el.simple_type.restriction:
                    enum = ir.EnumType()
                    enum.name = to_upper_camel_case(xml_el.simple_type.name)

                    for value in xml_el.simple_type.restriction.restrictions:
                        enum.enumerators.append(value.value)

                    state.enums.append(enum)

            if xml_el.complex_type:
                for xml_attr in xml_el.complex_type.attributes:
                    attribute = ir.Attribute()
                    attribute.name = to_camel_case(xml_attr.name)
                    attribute.xml_name = xml_attr.name
                    if xml_attr.type:
                        attribute.type, attribute.serde_type = map_type(state, xml_attr.type)
                        attribute.full_type = attribute.type
                    else:
                        if xml_attr.simple_type:
                            attribute.type = to_upper_camel_case(xml_attr.simple_type.name)
                            attribute.full_type = f"{cpp_namespace}::{attribute.type}"
                            attribute.serde_type = f"StringSerDe<{attribute.full_type}>"
                        else:
                            raise ValueError("Don't understand xml attribute")
                    attribute.required = (xml_attr.use == xsd.Attribute.UseType.Required)
                    el.attributes.append(attribute)

                if xml_el.complex_type.sequence:
                    sequence: xsd.Sequence = xml_el.complex_type.sequence
                    for xml_sub_el in sequence.elements:
                        sub_el = ir.SubElement()
                        sub_el.min_occurs = xml_sub_el.min_occurs
                        sub_el.max_occurs = xml_sub_el.max_occurs
                        sub_el.xmlns = xmlns
                        sub_el.xml_name = xml_sub_el.ref if xml_sub_el.ref else str(xml_sub_el.name)
                        # attribute name generation
                        sub_el.name = to_camel_case(sub_el.xml_name)
                        if sub_el.type_occurrence == ir.Occurrence.Vector:
                            sub_el.name += "s"

                        if xml_sub_el.complex_type or xml_sub_el.simple_type or xml_sub_el.ref:
                            sub_el.type = to_upper_camel_case(sub_el.xml_name)
                            sub_el.full_type = state.namespaces[xmlns].namespace + "::" + sub_el.type
                            sub_el.serde_type = f"XmlSerDe<{sub_el.full_type}>"
                        elif xml_sub_el.type:
                            # inline this element into struct
                            sub_el.type, content_serde_type = map_type(state, xml_sub_el.type)
                            sub_el.full_type = sub_el.type
                            sub_el.serde_type = f"XmlUtils::ElementContentSerDe<{content_serde_type}, u\"{sub_el.xml_name}\">"
                        else:
                            raise ValueError(f"Don't understand sub element: {sub_el}")
                        el.sub_els.append(sub_el)

            state.elements.append(el)

        case xsd.Attribute() as attr:
            attr: xsd.Attribute
            if attr.simple_type:
                if attr.simple_type.restriction:
                    enum = ir.EnumType()
                    enum.xmlns = xmlns
                    enum.namespace = state.namespaces[xmlns].namespace
                    enum.name = to_upper_camel_case(attr.simple_type.name)

                    for value in attr.simple_type.restriction.restrictions:
                        enum.xml_enumerators.append(value.value)
                    enum.enumerators = list(to_upper_camel_case(xml_enum) for xml_enum in enum.xml_enumerators)

                    state.enums.append(enum)

    return state


def generate_ir(components: list[xsd.Component]) -> State:
    state = State()

    for component in components:
        ns = ir.ComponentNamespace()
        ns.xmlns = component.target_xmlns
        ns.namespace = component.rename_prefix
        state.namespaces[ns.xmlns] = ns

        component.traverse(state, lambda s, obj: collect_elements(s, obj, ns.xmlns))
        break

    return state
