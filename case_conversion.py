# SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

def parse_minus_case(identifier: str) -> list[str]:
    return [word.lower() for word in identifier.split("-")]


def to_camel_case(identifier: str) -> str:
    words = parse_minus_case(identifier)
    out = words[0]
    for word in words[1:]:
        out += word[0].upper() + word[1:]
    return out


def to_upper_camel_case(identifier: str) -> str:
    out = str()
    for word in parse_minus_case(identifier):
        out += word[0].upper() + word[1:]
    return out
