#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Linus Jahn <lnj@kaidan.im>
# SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

import sys
from pathlib import Path
from xml.etree import ElementTree

import irgen
import xsd
import codegen


def generate_qt_classes(schema_dir: str):
    schema_path = Path(schema_dir)
    components: list[xsd.Component] = []
    for file in schema_path.iterdir():
        if not file.is_file():
            continue

        tree = ElementTree.parse(file)
        component = xsd.Component.from_xml(tree)
        if component:
            components.append(component)

    state: irgen.State = irgen.generate_ir(components)

    print("\nIR:")
    for e in state.elements:
        print(e)

    print("Generating code…")
    codegen.generate_code(state.elements, state.enums, state.namespaces)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(f"Usage: {sys.argv[0]} [xsd include path]")
        exit(1)

    generate_qt_classes(sys.argv[1])
