# SPDX-FileCopyrightText: 2022 Linus Jahn <lnj@kaidan.im>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

from pathlib import Path
from typing import TextIO, Iterable

import ir
from ir import Element, SubElement, EnumType

INDENT_WIDTH: int = 4
QT_TYPES = [
    "QString",
    "QByteArray",
]


def unique(iterable: Iterable[any]) -> Iterable[any]:
    s = sorted(iterable)
    last = None
    for x in s:
        if x != last:
            yield x
        last = x


def str_sum(iterable: Iterable[str]) -> str:
    out = str()
    for x in iterable:
        out += x
    return out


def indent(level: int):
    return level * INDENT_WIDTH * " "


def writeln(f: TextIO, text: str = str(), level: int = 0):
    f.write(indent(level) + text + "\n")


def write_autogen(f: TextIO):
    writeln(f, "// This file is auto-generated. All changes will vanish!")
    writeln(f)


def write_pragma_once(f: TextIO):
    writeln(f, "#pragma once")
    writeln(f)


def map_type_to_include(type: str) -> str:
    match type:
        case "std::optional":
            return "<optional>"
        case "std::vector":
            return "<vector>"
        case "QString":
            return "<QString>"
        case "int8_t" | "uint8_t" | "int16_t" | "uint16_t" | "int32_t" | "uint32_t" | "int64_t" | "uint64_t":
            return "<cstdint>"
    return f'"{type}.h"'


def include_types_private(el: Element) -> Iterable[str]:
    types = [el.name]
    for sub_el in el.sub_els:
        types.append(sub_el.serde_type)
    for attr in el.attributes:
        types.append(attr.serde_type)

    return unique(types)


def include_types_pub(el: Element) -> Iterable[str]:
    types: list[str] = []
    attrs: list[any] = el.attributes + el.sub_els + ([el.content] if el.content else [])

    for a in attrs:
        types.append(a.type)
        if a.type_occurrence == ir.Occurrence.Optional:
            types.append("std::optional")
        if a.type_occurrence == ir.Occurrence.Vector:
            types.append("std::vector")

    return unique(types)


def write_include(name: str, f: TextIO):
    writeln(f, f"#include {name}")


def write_all_includes(includes: list[str], f: TextIO):
    def write_include_group(incls: list[str], f: TextIO):
        for incl in incls:
            write_include(incl, f)
        if len(incls):
            writeln(f)

    global_includes = sorted(unique(filter(lambda x: x[0] == "<", includes)))
    local_includes = sorted(unique(filter(lambda x: x[0] == '"', includes)))

    write_include_group(global_includes, f)
    write_include_group(local_includes, f)


def write_pub_includes(el: Element, f: TextIO):
    includes = [map_type_to_include(t) for t in include_types_pub(el)] + ["\"xmlserde.h\""]
    write_all_includes(includes, f)


def write_private_includes(els: list[Element], enums: list[EnumType], f: TextIO):
    types = sorted(list(unique(a.name for a in (els + enums))))
    write_all_includes(list(map_type_to_include(t) for t in types) + ["\"serde_helpers.h\""], f)


def write_begin_namespace(namespace: str, f: TextIO):
    writeln(f, f"namespace {namespace} {{")
    writeln(f)


def write_end_namespace(namespace: str, f: TextIO):
    writeln(f, f"}}  // {namespace}")
    writeln(f)


def write_struct(el: Element, f: TextIO):
    def write_attr(type: str, name: str):
        writeln(f, f"{type} {name};", level=1)

    writeln(f, f"struct {el.name} {{")
    for attr in el.attributes:
        write_attr(attr.store_type, attr.name)
    if el.content:
        write_attr(el.content.type, el.content.name)
    for subel in el.sub_els:
        write_attr(subel.store_type, subel.name)
    writeln(f, "};")
    writeln(f)


def write_element_serializer(f: TextIO, el: Element):
    writeln(f, f"void XmlSerDe<{el.full_name}>::serialize(const {el.full_name} &d, QXmlStreamWriter &w) {{")
    writeln(f, f"w.writeStartElement(QStringLiteral(\"{el.xml_name}\"));", 1)
    writeln(f, f"w.writeDefaultNamespace(QStringLiteral(\"{el.xmlns}\"));", 1)
    for attribute in el.attributes:
        writeln(
            f,
            f"XmlUtils::serialize_attribute<XmlUtils::{attribute.type_occurrence.to_string()}, {attribute.full_type}, {attribute.serde_type}>(d.{attribute.name}, QStringLiteral(\"{attribute.xml_name}\"), w);", 1)
    if el.content:
        writeln(f, f"XmlUtils::serialize_content<XmlUtils::{el.content.type_occurrence.to_string()}, {el.content.type}, {el.content.serde_type}>(d.{el.content.name}, w);", 1)
    for sub_el in el.sub_els:
        writeln(f, f"XmlUtils::serialize<XmlUtils::{sub_el.type_occurrence.to_string()}, {sub_el.full_type}, {sub_el.serde_type}>(d.{sub_el.name}, w);", 1)
    writeln(f, f"w.writeEndElement();", 1)
    writeln(f, "}")
    writeln(f)


def write_element_deserializer(f: TextIO, el: Element):
    writeln(f, f"DeserializeResult<{el.full_name}> XmlSerDe<{el.full_name}>::deserialize(const QDomElement &input) {{")

    required_attributes = str_sum(
        (f"u\"{a.xml_name}\", " if a.required else "")
        for a in el.attributes
    )
    if len(required_attributes) > 0:
        writeln(f, f"if (auto err = XmlUtils::check_missing_attributes(input, {{ {required_attributes}}}))", 1)
        writeln(f, f"return *err;", 2)
        writeln(f)

    for attribute in el.attributes:
        if attribute.required:
            writeln(f, f"auto {attribute.name}Result = {attribute.serde_type}::deserialize(input.attribute(QStringLiteral(\"{attribute.xml_name}\")));", 1)
            writeln(f, f"if (std::holds_alternative<XmlDeserializeError>({attribute.name}Result))", 1)
            writeln(f, f"return std::get<XmlDeserializeError>(std::move({attribute.name}Result));", 2)
        else:
            # optional
            writeln(f, f"{attribute.store_type} {attribute.name};", 1)
            writeln(f, f"if (input.hasAttribute(QStringLiteral(\"{attribute.xml_name}\"))) {{", 1)
            writeln(f, f"auto {attribute.name}Result = {attribute.serde_type}::deserialize(input.attribute(QStringLiteral(\"{attribute.xml_name}\")));", 2)
            writeln(f, f"if (std::holds_alternative<XmlDeserializeError>({attribute.name}Result))", 2)
            writeln(f, f"return std::get<XmlDeserializeError>(std::move({attribute.name}Result));", 3)
            writeln(f, f"{attribute.name} = std::get<{attribute.type}>(std::move({attribute.name}Result));", 2)
            writeln(f, f"}}", 1)
        writeln(f)

    if el.content:
        if el.content.required:
            result_var = f"{el.content.name}Result"

            writeln(f, "if (input.text().isEmpty())", 1)
            writeln(f, f"return XmlDeserializeError {{ QStringLiteral(\"{el.full_name}: Missing required element content!\"), XmlDeserializeError::MissingContent }};", 2)
            writeln(f, f"auto {result_var} = {el.content.serde_type}::deserialize(input.text());", 1)
            writeln(f, f"if (std::holds_alternative<XmlDeserializeError>({result_var}))", 1)
            writeln(f, f"return std::get<XmlDeserializeError>(std::move({result_var}));", 2)
        else:
            writeln(f, f"{el.content.store_type} {el.content.name};", 1)
            writeln(f, f"if (auto text = input.text(); !text.isEmpty()) {{", 1)
            writeln(f, f"auto contentResult = {el.content.serde_type}::deserialize(text);", 2)
            writeln(f, f"if (std::holds_alternative<XmlDeserializeError>(contentResult))", 2)
            writeln(f, f"return std::get<XmlDeserializeError>(std::move(contentResult));", 3)
            writeln(f, f"{el.content.name} = std::get<{el.content.type}>(std::move(contentResult));", 2)
            writeln(f, "}}", 1)
        writeln(f)

    for subel in el.sub_els:
        writeln(f, f"auto {subel.name}Result = XmlUtils::deserialize<XmlUtils::{subel.type_occurrence.to_string()}, "
                   f"{subel.full_type}, {subel.serde_type}, {subel.min_occurs}, {subel.max_occurs}>(input, "
                   f"u\"{subel.xml_name}\", u\"{subel.xmlns}\");", 1)
        writeln(f, f"if (std::holds_alternative<XmlDeserializeError>({subel.name}Result))", 1)
        writeln(f, f"return std::get<XmlDeserializeError>(std::move({subel.name}Result));", 2)

    f.write(indent(1) + f"return {el.full_name} {{ ")
    for a in el.attributes:
        if a.required:
            f.write(f"std::get<{a.full_type}>(std::move({a.name}Result)), ")
        else:
            f.write(f"{a.name}, ")
    if el.content:
        if el.content.required:
            f.write(f"std::get<{el.content.type}>(std::move({el.content.name}Result)), ")
        else:
            f.write(f"{el.content.name}, ")
    for sub_el in el.sub_els:
        f.write(f"std::get<{sub_el.store_type}>(std::move({sub_el.name}Result)), ")
    writeln(f, "};")
    writeln(f, "}")
    writeln(f)


def write_element_serde_header(el: Element, f: TextIO):
    f.write(f"""template <>
struct XmlSerDe<{el.full_name}> {{
    static void serialize(const {el.full_name} &d, QXmlStreamWriter &w);
    static DeserializeResult<{el.full_name}> deserialize(const QDomElement &input);
}};
""")


def write_element_header(el: Element, out_dir: Path):
    f = out_dir / (el.name + ".h")
    # assert not f.exists()

    io = f.open("w")
    write_autogen(io)
    write_pragma_once(io)
    write_pub_includes(el, io)

    write_begin_namespace(el.namespace, io)
    write_struct(el, io)
    write_end_namespace(el.namespace, io)
    write_element_serde_header(el, io)


def write_enum(enum: EnumType, f: TextIO):
    write_begin_namespace(enum.namespace, f)
    writeln(f, f"enum {enum.name} {{ {', '.join(enum.enumerators)} }};")
    writeln(f)
    write_end_namespace(enum.namespace, f)


def write_enum_header(enum: EnumType, out_dir: Path):
    f = out_dir / (enum.name + ".h")

    io = f.open("w")
    write_autogen(io)
    write_pragma_once(io)
    write_enum(enum, io)
    write_enum_serde_header(enum, io)


def write_enum_serde_header(enum: EnumType, f: TextIO):
    f.write(f"""template <>
struct StringSerDe<{enum.full_name}> {{
    static QString serialize({enum.full_name});
    static DeserializeResult<{enum.full_name}> deserialize(QStringView);
}};
""")


def write_enum_serializer(f: TextIO, enum: EnumType):
    writeln(f, f"QString StringSerDe<{enum.full_name}>::serialize({enum.full_name} e) {{")
    writeln(f, "switch (e) {", 1)
    for i in range(len(enum.enumerators)):
        writeln(f, f"case {enum.full_name}::{enum.enumerators[i]}:", 1)
        writeln(f, f"return QStringLiteral(\"{enum.xml_enumerators[i]}\");", 2)

    writeln(f, "};", 1)
    writeln(f, "}")
    writeln(f)


def write_enum_deserializer(f: TextIO, enum: EnumType):
    writeln(f, f"DeserializeResult<{enum.full_name}> StringSerDe<{enum.full_name}>::deserialize(QStringView input) {{")
    for i in range(len(enum.enumerators)):
        writeln(f, f"if (input == u\"{enum.xml_enumerators[i]}\")", 1)
        writeln(f, f"return {enum.full_name}::{enum.enumerators[i]};", 2)
    writeln(f, f"return XmlDeserializeError {{ u\"{enum.full_name}: Invalid enum value \\\"\" % input % u\"\\\".\", XmlDeserializeError::InvalidEnumValue }};", 1)
    writeln(f, "}")
    writeln(f)


def write_enum_serde_impl(f: TextIO, enum: EnumType):
    write_enum_serializer(f, enum)
    write_enum_deserializer(f, enum)


def generate_cpp(els: list[Element], enums: list[EnumType], out_dir: Path, namespace: ir.ComponentNamespace):
    cpp_namespace = namespace.namespace
    filepath = out_dir / (cpp_namespace + ".cpp")

    f = filepath.open("w")
    write_autogen(f)
    write_private_includes(els, enums, f)
    for enum in enums:
        write_enum_serde_impl(f, enum)
    for el in els:
        write_element_serializer(f, el)
        write_element_deserializer(f, el)


def generate_code(elements: list[Element], enums: list[EnumType], namespaces: dict[str, ir.ComponentNamespace]):
    outdir = Path("./codegen")
    if not outdir.exists():
        outdir.mkdir()

    for element in elements:
        write_element_header(element, outdir)

    for enum in enums:
        write_enum_header(enum, outdir)

    for xmlns in namespaces:
        ns_els = list(filter(lambda e: e.xmlns == xmlns, elements))
        ns_enums = list(filter(lambda e: e.xmlns == xmlns, enums))
        generate_cpp(ns_els, ns_enums, outdir, namespaces[xmlns])
