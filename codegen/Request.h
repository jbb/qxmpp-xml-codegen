// This file is auto-generated. All changes will vanish!

#pragma once

#include <QString>
#include <cstdint>
#include <optional>

#include "xmlserde.h"

namespace HttpUpload {

struct Request {
    QString filename;
    uint64_t size;
    std::optional<QString> contentType;
};

}  // HttpUpload

template <>
struct XmlSerDe<HttpUpload::Request> {
    static void serialize(const HttpUpload::Request &d, QXmlStreamWriter &w);
    static DeserializeResult<HttpUpload::Request> deserialize(const QDomElement &input);
};
