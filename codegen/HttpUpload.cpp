// This file is auto-generated. All changes will vanish!

#include "FileTooLarge.h"
#include "Get.h"
#include "Header.h"
#include "HttpHeader.h"
#include "Put.h"
#include "Request.h"
#include "Retry.h"
#include "Slot.h"
#include "serde_helpers.h"

QString StringSerDe<HttpUpload::HttpHeader>::serialize(HttpUpload::HttpHeader e) {
    switch (e) {
    case HttpUpload::HttpHeader::Authorization:
        return QStringLiteral("Authorization");
    case HttpUpload::HttpHeader::Cookie:
        return QStringLiteral("Cookie");
    case HttpUpload::HttpHeader::Expires:
        return QStringLiteral("Expires");
    };
}

DeserializeResult<HttpUpload::HttpHeader> StringSerDe<HttpUpload::HttpHeader>::deserialize(QStringView input) {
    if (input == u"Authorization")
        return HttpUpload::HttpHeader::Authorization;
    if (input == u"Cookie")
        return HttpUpload::HttpHeader::Cookie;
    if (input == u"Expires")
        return HttpUpload::HttpHeader::Expires;
    return XmlDeserializeError { u"HttpUpload::HttpHeader: Invalid enum value \"" % input % u"\".", XmlDeserializeError::InvalidEnumValue };
}

void XmlSerDe<HttpUpload::Request>::serialize(const HttpUpload::Request &d, QXmlStreamWriter &w) {
    w.writeStartElement(QStringLiteral("request"));
    w.writeDefaultNamespace(QStringLiteral("urn:xmpp:http:upload:0"));
    XmlUtils::serialize_attribute<XmlUtils::Single, QString, StringSerDe<QString>>(d.filename, QStringLiteral("filename"), w);
    XmlUtils::serialize_attribute<XmlUtils::Single, uint64_t, IntegerSerDe<uint64_t>>(d.size, QStringLiteral("size"), w);
    XmlUtils::serialize_attribute<XmlUtils::Optional, QString, StringSerDe<QString>>(d.contentType, QStringLiteral("content-type"), w);
    w.writeEndElement();
}

DeserializeResult<HttpUpload::Request> XmlSerDe<HttpUpload::Request>::deserialize(const QDomElement &input) {
    if (auto err = XmlUtils::check_missing_attributes(input, { u"filename", u"size", }))
        return *err;

    auto filenameResult = StringSerDe<QString>::deserialize(input.attribute(QStringLiteral("filename")));
    if (std::holds_alternative<XmlDeserializeError>(filenameResult))
        return std::get<XmlDeserializeError>(std::move(filenameResult));

    auto sizeResult = IntegerSerDe<uint64_t>::deserialize(input.attribute(QStringLiteral("size")));
    if (std::holds_alternative<XmlDeserializeError>(sizeResult))
        return std::get<XmlDeserializeError>(std::move(sizeResult));

    std::optional<QString> contentType;
    if (input.hasAttribute(QStringLiteral("content-type"))) {
        auto contentTypeResult = StringSerDe<QString>::deserialize(input.attribute(QStringLiteral("content-type")));
        if (std::holds_alternative<XmlDeserializeError>(contentTypeResult))
            return std::get<XmlDeserializeError>(std::move(contentTypeResult));
        contentType = std::get<QString>(std::move(contentTypeResult));
    }

    return HttpUpload::Request { std::get<QString>(std::move(filenameResult)), std::get<uint64_t>(std::move(sizeResult)), contentType, };
}

void XmlSerDe<HttpUpload::Slot>::serialize(const HttpUpload::Slot &d, QXmlStreamWriter &w) {
    w.writeStartElement(QStringLiteral("slot"));
    w.writeDefaultNamespace(QStringLiteral("urn:xmpp:http:upload:0"));
    XmlUtils::serialize<XmlUtils::Single, HttpUpload::Put, XmlSerDe<HttpUpload::Put>>(d.put, w);
    XmlUtils::serialize<XmlUtils::Single, HttpUpload::Get, XmlSerDe<HttpUpload::Get>>(d.get, w);
    w.writeEndElement();
}

DeserializeResult<HttpUpload::Slot> XmlSerDe<HttpUpload::Slot>::deserialize(const QDomElement &input) {
    auto putResult = XmlUtils::deserialize<XmlUtils::Single, HttpUpload::Put, XmlSerDe<HttpUpload::Put>, 1, 1>(input, u"put", u"urn:xmpp:http:upload:0");
    if (std::holds_alternative<XmlDeserializeError>(putResult))
        return std::get<XmlDeserializeError>(std::move(putResult));
    auto getResult = XmlUtils::deserialize<XmlUtils::Single, HttpUpload::Get, XmlSerDe<HttpUpload::Get>, 1, 1>(input, u"get", u"urn:xmpp:http:upload:0");
    if (std::holds_alternative<XmlDeserializeError>(getResult))
        return std::get<XmlDeserializeError>(std::move(getResult));
    return HttpUpload::Slot { std::get<HttpUpload::Put>(std::move(putResult)), std::get<HttpUpload::Get>(std::move(getResult)), };
}

void XmlSerDe<HttpUpload::Put>::serialize(const HttpUpload::Put &d, QXmlStreamWriter &w) {
    w.writeStartElement(QStringLiteral("put"));
    w.writeDefaultNamespace(QStringLiteral("urn:xmpp:http:upload:0"));
    XmlUtils::serialize_attribute<XmlUtils::Single, QString, StringSerDe<QString>>(d.url, QStringLiteral("url"), w);
    XmlUtils::serialize<XmlUtils::Vector, HttpUpload::Header, XmlSerDe<HttpUpload::Header>>(d.headers, w);
    w.writeEndElement();
}

DeserializeResult<HttpUpload::Put> XmlSerDe<HttpUpload::Put>::deserialize(const QDomElement &input) {
    if (auto err = XmlUtils::check_missing_attributes(input, { u"url", }))
        return *err;

    auto urlResult = StringSerDe<QString>::deserialize(input.attribute(QStringLiteral("url")));
    if (std::holds_alternative<XmlDeserializeError>(urlResult))
        return std::get<XmlDeserializeError>(std::move(urlResult));

    auto headersResult = XmlUtils::deserialize<XmlUtils::Vector, HttpUpload::Header, XmlSerDe<HttpUpload::Header>, 0, -1>(input, u"header", u"urn:xmpp:http:upload:0");
    if (std::holds_alternative<XmlDeserializeError>(headersResult))
        return std::get<XmlDeserializeError>(std::move(headersResult));
    return HttpUpload::Put { std::get<QString>(std::move(urlResult)), std::get<std::vector<HttpUpload::Header>>(std::move(headersResult)), };
}

void XmlSerDe<HttpUpload::Header>::serialize(const HttpUpload::Header &d, QXmlStreamWriter &w) {
    w.writeStartElement(QStringLiteral("header"));
    w.writeDefaultNamespace(QStringLiteral("urn:xmpp:http:upload:0"));
    XmlUtils::serialize_attribute<XmlUtils::Single, HttpUpload::HttpHeader, StringSerDe<HttpUpload::HttpHeader>>(d.name, QStringLiteral("name"), w);
    XmlUtils::serialize_content<XmlUtils::Single, QString, StringSerDe<QString>>(d.value, w);
    w.writeEndElement();
}

DeserializeResult<HttpUpload::Header> XmlSerDe<HttpUpload::Header>::deserialize(const QDomElement &input) {
    if (auto err = XmlUtils::check_missing_attributes(input, { u"name", }))
        return *err;

    auto nameResult = StringSerDe<HttpUpload::HttpHeader>::deserialize(input.attribute(QStringLiteral("name")));
    if (std::holds_alternative<XmlDeserializeError>(nameResult))
        return std::get<XmlDeserializeError>(std::move(nameResult));

    if (input.text().isEmpty())
        return XmlDeserializeError { QStringLiteral("HttpUpload::Header: Missing required element content!"), XmlDeserializeError::MissingContent };
    auto valueResult = StringSerDe<QString>::deserialize(input.text());
    if (std::holds_alternative<XmlDeserializeError>(valueResult))
        return std::get<XmlDeserializeError>(std::move(valueResult));

    return HttpUpload::Header { std::get<HttpUpload::HttpHeader>(std::move(nameResult)), std::get<QString>(std::move(valueResult)), };
}

void XmlSerDe<HttpUpload::Get>::serialize(const HttpUpload::Get &d, QXmlStreamWriter &w) {
    w.writeStartElement(QStringLiteral("get"));
    w.writeDefaultNamespace(QStringLiteral("urn:xmpp:http:upload:0"));
    XmlUtils::serialize_attribute<XmlUtils::Single, QString, StringSerDe<QString>>(d.url, QStringLiteral("url"), w);
    w.writeEndElement();
}

DeserializeResult<HttpUpload::Get> XmlSerDe<HttpUpload::Get>::deserialize(const QDomElement &input) {
    if (auto err = XmlUtils::check_missing_attributes(input, { u"url", }))
        return *err;

    auto urlResult = StringSerDe<QString>::deserialize(input.attribute(QStringLiteral("url")));
    if (std::holds_alternative<XmlDeserializeError>(urlResult))
        return std::get<XmlDeserializeError>(std::move(urlResult));

    return HttpUpload::Get { std::get<QString>(std::move(urlResult)), };
}

void XmlSerDe<HttpUpload::FileTooLarge>::serialize(const HttpUpload::FileTooLarge &d, QXmlStreamWriter &w) {
    w.writeStartElement(QStringLiteral("file-too-large"));
    w.writeDefaultNamespace(QStringLiteral("urn:xmpp:http:upload:0"));
    XmlUtils::serialize<XmlUtils::Optional, uint64_t, XmlUtils::ElementContentSerDe<IntegerSerDe<uint64_t>, u"max-file-size">>(d.maxFileSize, w);
    w.writeEndElement();
}

DeserializeResult<HttpUpload::FileTooLarge> XmlSerDe<HttpUpload::FileTooLarge>::deserialize(const QDomElement &input) {
    auto maxFileSizeResult = XmlUtils::deserialize<XmlUtils::Optional, uint64_t, XmlUtils::ElementContentSerDe<IntegerSerDe<uint64_t>, u"max-file-size">, 0, 1>(input, u"max-file-size", u"urn:xmpp:http:upload:0");
    if (std::holds_alternative<XmlDeserializeError>(maxFileSizeResult))
        return std::get<XmlDeserializeError>(std::move(maxFileSizeResult));
    return HttpUpload::FileTooLarge { std::get<std::optional<uint64_t>>(std::move(maxFileSizeResult)), };
}

void XmlSerDe<HttpUpload::Retry>::serialize(const HttpUpload::Retry &d, QXmlStreamWriter &w) {
    w.writeStartElement(QStringLiteral("retry"));
    w.writeDefaultNamespace(QStringLiteral("urn:xmpp:http:upload:0"));
    XmlUtils::serialize_attribute<XmlUtils::Single, QString, StringSerDe<QString>>(d.stamp, QStringLiteral("stamp"), w);
    w.writeEndElement();
}

DeserializeResult<HttpUpload::Retry> XmlSerDe<HttpUpload::Retry>::deserialize(const QDomElement &input) {
    if (auto err = XmlUtils::check_missing_attributes(input, { u"stamp", }))
        return *err;

    auto stampResult = StringSerDe<QString>::deserialize(input.attribute(QStringLiteral("stamp")));
    if (std::holds_alternative<XmlDeserializeError>(stampResult))
        return std::get<XmlDeserializeError>(std::move(stampResult));

    return HttpUpload::Retry { std::get<QString>(std::move(stampResult)), };
}

