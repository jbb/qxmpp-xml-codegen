// This file is auto-generated. All changes will vanish!

#pragma once

#include <QString>

#include "HttpHeader.h"
#include "xmlserde.h"

namespace HttpUpload {

struct Header {
    HttpHeader name;
    QString value;
};

}  // HttpUpload

template <>
struct XmlSerDe<HttpUpload::Header> {
    static void serialize(const HttpUpload::Header &d, QXmlStreamWriter &w);
    static DeserializeResult<HttpUpload::Header> deserialize(const QDomElement &input);
};
