# SPDX-FileCopyrightText: 2022 Linus Jahn <lnj@kaidan.im>
# SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

from collections.abc import Iterable
from json import JSONEncoder, dumps
from xml.etree import ElementTree
from typing import Optional, Callable

from enum import Enum, auto


TRAVERSE_DEBUGGING = True


def tdbg(text: str):
    if TRAVERSE_DEBUGGING:
        print(f"traverse::{text}")


def xs_xmlns(base: str) -> str:
    return "{http://www.w3.org/2001/XMLSchema}" + base


def qx_xmlns(base: str) -> str:
    return "{org:qxmpp:xmlcodegen:0}" + base


def dump(obj: any) -> str:
    class JsonEncoder(JSONEncoder):
        def default(self, o):
            if o.__class__.__name__ == "mappingproxy":
                return str()
            return o.__dict__

    return obj.__class__.__name__ + " " + dumps(obj.__dict__, indent=2, cls=JsonEncoder)


def remove_none(input: Iterable[Optional[any]]) -> Iterable[any]:
    return filter(lambda x: x, input)


def iter_elements(input: ElementTree.Element, tag: str) -> Iterable[ElementTree.Element]:
    return filter(lambda el: el.tag == tag, input)


class Enumeration:
    value: str

    @staticmethod
    def from_xml(el: ElementTree.Element) -> Optional[any]:
        e = Enumeration()
        e.value = el.attrib["value"]
        return e

    def traverse(self, state, fun: Callable) -> any:
        pass

    def __str__(self):
        return str(self.__dict__)


class Restriction:
    base: str
    restrictions: list[Enumeration]

    @staticmethod
    def from_xml(el: ElementTree.Element) -> Optional[any]:
        r = Restriction()
        r.base = el.attrib["base"]
        r.restrictions = []
        for el in iter_elements(el, xs_xmlns("enumeration")):
            e = Enumeration.from_xml(el)
            if e:
                r.restrictions.append(e)

        return r

    def traverse(self, state, fun: Callable) -> any:
        pass

    def __str__(self):
        return dump(self)


class SimpleType:
    restriction: Optional[Restriction] = None
    name: str

    @staticmethod
    def from_xml(el: ElementTree.Element) -> Optional[any]:
        s = SimpleType()
        for sub_el in iter_elements(el, xs_xmlns("restriction")):
            s.restriction = Restriction.from_xml(sub_el)

        try:
            s.name = el.attrib["name"]
        except KeyError:
            print("Error: Name of a simple type may not be empty")
            raise ValueError

        if s.name == "":
            print("Error: Name of a simple type may not be empty")
            raise ValueError

        return s

    def traverse(self, state, fun: Callable) -> any:
        pass

    def __str__(self):
        return dump(self)


class Attribute:
    class UseType(Enum):
        Required = auto()
        Optional = auto()
        Prohibited = auto()

    name: str
    use: UseType = UseType.Optional
    type: Optional[str] = None
    simple_type: Optional[SimpleType] = None

    def traverse(self, state, fun: Callable):
        state = fun(state, self)
        if self.simple_type:
            self.simple_type.traverse(state, fun)

    @staticmethod
    def parse_use_type(name: str) -> UseType:
        match name:
            case "required":
                return Attribute.UseType.Required
            case "optional":
                return Attribute.UseType.Optional
            case "prohibited":
                return Attribute.UseType.Prohibited
        raise NotImplementedError(f"Unhandled use type \"{name}\"")

    @staticmethod
    def from_xml(el: ElementTree.Element) -> Optional[any]:
        a = Attribute()
        a.name = el.attrib["name"]
        if "use" in el.attrib:
            a.use = Attribute.parse_use_type(el.attrib["use"])
        if "type" in el.attrib:
            a.type = el.attrib["type"]

        simple_types = list(iter_elements(el, xs_xmlns("simpleType")))
        if len(simple_types) > 1:
            print("Error: More than one simple type found in attribute")
        if len(simple_types) > 0:
            a.simple_type = SimpleType.from_xml(simple_types[0])
            assert a.simple_type
        return a

    def __str__(self):
        return dump(self)


class ComplexType:
    name: Optional[str] = None
    # actually Optional[Sequence]
    sequence: Optional[any] = None
    attributes: list[Attribute]

    def traverse(self, state, fun: Callable) -> any:
        state = fun(state, self)
        for e in self.attributes + [self.sequence]:
            if e:
                tdbg(f"ComplexType: {type(e)}")
                e.traverse(state, fun)

    @staticmethod
    def from_xml(tree: ElementTree.Element) -> Optional[any]:
        c = ComplexType()
        if "name" in tree.attrib:
            c.name = tree.attrib["name"]
        for seq in iter_elements(tree, xs_xmlns("sequence")):
            c.sequence = Sequence.from_xml(seq)
        c.attributes = list(remove_none(
            Attribute.from_xml(el) for el in iter_elements(tree, xs_xmlns("attribute"))
        ))

        return c

    def __str__(self):
        return dump(self)


class Element:
    name: Optional[str] = None
    ref: Optional[str] = None
    type: Optional[str] = None
    min_occurs: int = 1
    max_occurs: int = 1
    complex_type: Optional[ComplexType] = None
    simple_type: Optional[SimpleType] = None
    content_name: str = "content"

    def traverse(self, state, fun: Callable) -> any:
        fun(state, self)
        for e in [self.complex_type, self.simple_type]:
            if e:
                tdbg(f"Element: {type(e)}")
                e.traverse(state, fun)

    @staticmethod
    def from_xml(el: ElementTree.Element) -> Optional[any]:
        result = Element()

        if "name" in el.attrib:
            result.name = el.attrib["name"]
        if "ref" in el.attrib:
            result.ref = el.attrib["ref"]
        if "type" in el.attrib:
            result.type = el.attrib["type"]
        if "minOccurs" in el.attrib:
            result.min_occurs = int(el.attrib["minOccurs"])
        if "maxOccurs" in el.attrib:
            match el.attrib["maxOccurs"]:
                case "unbounded":
                    result.max_occurs = -1
                case str() as max_occurs:
                    result.max_occurs = int(max_occurs)
        if qx_xmlns("renameContent") in el.attrib:
            result.content_name = el.attrib[qx_xmlns("renameContent")]
        for complexType in iter_elements(el, xs_xmlns("complexType")):
            result.complex_type = ComplexType.from_xml(complexType)
        for simpleType in iter_elements(el, xs_xmlns("simpleType")):
            result.simple_type = SimpleType.from_xml(simpleType)
        return result

    def __str__(self):
        return dump(self)


class Component:
    target_xmlns: str
    rename_prefix: str
    elements: list[Element]
    simple_types: list[SimpleType]
    complex_types: list[ComplexType]

    def traverse(self, state, fun: Callable) -> any:
        fun(state, self)
        for e in self.elements + self.simple_types + self.complex_types:
            e.traverse(state, fun)

    @staticmethod
    def from_xml(tree: ElementTree) -> Optional[any]:
        root = tree.getroot()

        c = Component()
        c.target_xmlns = root.attrib["targetNamespace"]
        c.rename_prefix = root.attrib[qx_xmlns("renamePrefix")]
        c.elements = []
        for el in iter_elements(root, xs_xmlns("element")):
            c.elements.append(Element.from_xml(el))
        c.simple_types = list(remove_none(SimpleType.from_xml(el) for el in iter_elements(root, xs_xmlns("simpleType"))))
        c.complex_types = list(remove_none(ComplexType.from_xml(el) for el in iter_elements(root, xs_xmlns("complexType"))))

        return c

    def __str__(self):
        return dump(self)


class Sequence:
    elements: list[Element]

    def traverse(self, state, fun: Callable) -> any:
        state = fun(state, self)
        for e in self.elements:
            tdbg(f"Sequence: {type(e)}")
            e.traverse(state, fun)

    @staticmethod
    def from_xml(tree: ElementTree.Element) -> Optional[any]:
        c = Sequence()
        c.elements = list(remove_none(
            Element.from_xml(el) for el in iter_elements(tree, xs_xmlns("element"))
        ))

        return c

    def __str__(self):
        return dump(self)


class Type(Enum):
    PositiveInteger = auto()
    String = auto()
    AnyType = auto()


def parse_type(name: str) -> Type:
    match name:
        case "positiveInteger":
            return Type.PositiveInteger
        case "string":
            return Type.String
        case "anyType":
            return Type.AnyType
    raise NotImplementedError(f"Unhandled type \"{name}\"", name)
